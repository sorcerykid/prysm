Prysm Viewer Mod v1.0
By Leslie E. Krause

Prysm Viewer is a fully interactive tool for visualizing the output of the 2d perlin noise 
generator given different input parameters.

https://forum.minetest.net/viewtopic.php?f=9&t=23968

Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/prysm

Download archive...
  https://bitbucket.org/sorcerykid/prysm/get/master.zip
  https://bitbucket.org/sorcerykid/prysm/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.14+ required

Dependencies
----------------------

ActiveFormspecs Mod (required)
  https://bitbucket.org/sorcerykid/formspecs

Polygraph Mod (required)
  https://bitbucket.org/sorcerykid/polygraph

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the prysm-master directory to "prysm"

License of source code
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2018-2019, Leslie E. Krause

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html
